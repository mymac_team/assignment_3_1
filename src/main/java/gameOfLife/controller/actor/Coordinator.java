package gameOfLife.controller.actor;

import akka.actor.ActorRef;
import akka.actor.Props;
import gameOfLife.model.actor.AbstractIterateMatrixActor;
import gameOfLife.model.actor.UpdateMatrixActor;
import gameOfLife.model.counter.Counter;
import gameOfLife.model.counter.CounterImpl;
import gameOfLife.model.matrix.Matrix;
import gameOfLife.model.message.CellToUpdateMessageRequest;
import gameOfLife.model.message.CellToUpdateMessageResponse;
import gameOfLife.model.message.InitializeCellMessage;
import gameOfLife.model.message.Messages;
import gameOfLife.view.View;

/**
 * Coordinator actor between model and view.
 * It is responsible to initialize and it manages the update process.
 */
 public class Coordinator extends AbstractIterateMatrixActor {
    private static final int INCREMENT_VALUE = 1;
    private static final double FIFTY_PERCENT = 0.5;
    private static final int DEFAULT_INDEX = 0;
    private final Counter cellUpdateCounter;
    private ActorRef updaterActor;
    private View view;

    /**
     * Factory for a new coordinator actor.
     * @param view the view reference.
     * @return The Props.
     */
    public static Props props(View view) {
        return Props.create(Coordinator.class, () -> new Coordinator(view));
    }

    /**
     * Constructor for the coordinator actor.
     * @param view the view reference.
     */
    public Coordinator(View view) {
        super();
        this.view = view;
        this.cellUpdateCounter = new CounterImpl(super.getMatrix().getTotalCellsNumber());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void preStart() {
        this.updaterActor = getContext().actorOf(UpdateMatrixActor.props(getSelf()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Receive createReceive() {
        return this.getInitializationRequest();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void sendSelfMessage(int row, int column) {
        Matrix neighbourWorld = super.getMatrix().getNeighbourMatrix(row, column);
        getSelf().tell(new CellToUpdateMessageRequest(row, column, neighbourWorld), ActorRef.noSender());
    }

    private Receive getInitializationRequest() {
        return receiveBuilder()
                .match(InitializeCellMessage.class, message -> {
                    int row = message.getRow();
                    int column = message.getColumn();

                    super.getMatrix().updateStateIn(row, column, this.getRandomBoolean());

                    if (this.isLastCellReached(row, column)) {
                        this.view.notifyView(super.getMatrix());
                        getContext().become(this.getUpdateRequest());
                    } else if (super.isLastColumnReached(column)) {
                        getSelf().tell(new InitializeCellMessage(this.incrementIndex(row), DEFAULT_INDEX) , ActorRef.noSender());
                    } else {
                        getSelf().tell(new InitializeCellMessage(row, this.incrementIndex(column)) , ActorRef.noSender());
                    }
                })
                .build();
    }

    private Receive getUpdateRequest() {
        return receiveBuilder()
            .matchEquals(Messages.NEXT_EVOLUTION, messages -> {
                this.sendSelfMessage(DEFAULT_INDEX, DEFAULT_INDEX);
            }).match(CellToUpdateMessageRequest.class, message -> {
                this.notifyAndIterate(this.updaterActor, message);

                if(this.isLastCellReached(message.getRow(), message.getColumn())) {
                    getContext().become(this.getUpdateResponse());
                }

            }).matchEquals(Messages.STOP_EVOLUTION, messages -> {
                this.view.notifyView(super.getMatrix());
                getContext().become(this.getInitializationRequest());
            }).build();
    }

    private Receive getUpdateResponse() {
        return receiveBuilder()
                .match(CellToUpdateMessageResponse.class, message -> {
                    super.getMatrix().updateStateIn(message.getRow(), message.getColumn(), message.getState());
                    this.cellUpdateCounter.decrement();

                    if (message.getState()) {
                        super.getMatrix().incrementAliveCellsNumber();
                    }

                    if (this.cellUpdateCounter.isReached(0)) {
                        this.view.notifyView(super.getMatrix());
                        this.cellUpdateCounter.setValue(super.getMatrix().getTotalCellsNumber());
                        getContext().become(this.getUpdateRequest());
                    }
                })
                .matchEquals(Messages.STOP_EVOLUTION, messages -> {
                    this.view.notifyView(super.getMatrix());
                    getContext().become(this.getInitializationRequest());
                })
                .build();
    }

    private boolean isLastCellReached(int row, int column) {
        return row == super.getMatrix().getSize() - 1
                && column == super.getMatrix().getSize() - 1;
    }

    private int incrementIndex(int index) {
        return index + INCREMENT_VALUE;
    }

    private boolean getRandomBoolean() {
        return Math.random() < FIFTY_PERCENT;
    }
}
