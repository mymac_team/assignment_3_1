package gameOfLife;

import gameOfLife.view.MainFrame;
import gameOfLife.view.View;

public class Application {

    private static final int SIZE = 100;

    public static void main(final String[] args) {
        View gameView = new MainFrame();
        gameView.open();
    }

    public static int getSize() {
        return SIZE;
    }
}
