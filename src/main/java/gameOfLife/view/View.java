package gameOfLife.view;

import gameOfLife.model.matrix.Matrix;

public interface View {
    void open();

    void close();

    void notifyView(Matrix matrix);

    void stopAction();
}
