package gameOfLife.view;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import gameOfLife.controller.actor.Coordinator;
import gameOfLife.model.matrix.Matrix;
import gameOfLife.model.message.InitializeCellMessage;
import gameOfLife.model.message.Messages;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainFrame extends JFrame implements View {

    private static final String CELLS_ALIVE = "Cells alive: ";
    private static final String TITLE = "Game of Life";
    private static final String ACTOR_SYSTEM_NAME = "WorldActorSystem";
    private static final String COORDINATOR_ACTOR_NAME = "CoordinatorActor";
    private static final String START = "Start";
    private static final String STOP = "Stop";
    private static final int SIZE = 500;
    private MatrixPanel matrixPanel;
    private JScrollPane scrollPane;
    private JButton start;
    private JButton stop;
    private JLabel aliveCellsLabel;
    private ActorRef coordinatorActor;

    public MainFrame(){
        super(TITLE);

        this.setFrameProperties();
        this.setActorEnvironment();
        this.setMatrixPanel();
        this.setScrollPane();
        this.setContentPane();

        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent ev){
                close();
            }
            public void windowClosed(WindowEvent ev){
                close();
            }
        });
    }

    @Override
    public void open() {
        this.stop.setEnabled(false);
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void close() {
        this.dispose();
    }

    @Override
    public void notifyView(Matrix matrix) {
        SwingUtilities.invokeLater(() -> {
            this.aliveCellsLabel.setText(CELLS_ALIVE +
                    matrix.getAliveCellsNumber());
            matrix.resetAliveCellsNumber();
            this.matrixPanel.updateModel(matrix);
            this.scrollPane.repaint();
        });
    }

    @Override
    public void stopAction() {
        start.setEnabled(false);
        this.stop.setEnabled(false);
        this.coordinatorActor.tell(Messages.STOP_EVOLUTION, ActorRef.noSender());
    }

    private void setFrameProperties() {
        this.setSize(SIZE, SIZE);
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void setContentPane() {
        Container contentPane = getContentPane();
        contentPane.add(this.scrollPane, BorderLayout.CENTER);
        contentPane.add(this.setInteractionPanel(), BorderLayout.SOUTH);
    }

    private void setMatrixPanel() {
        this.matrixPanel = new MatrixPanel(this.coordinatorActor);
        this.matrixPanel.setPreferredSize(
                new Dimension(this.matrixPanel.getMatrixSideSize(), this.matrixPanel.getMatrixSideSize()));
    }

    private void setScrollPane() {
        this.scrollPane = new JScrollPane(this.matrixPanel);
        this.scrollPane.setAutoscrolls(true);
        this.scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    }

    private JPanel setInteractionPanel() {
        JPanel buttonPane = new JPanel();
        this.start = new JButton(START);
        this.stop  = new JButton(STOP);
        this.aliveCellsLabel = new JLabel(CELLS_ALIVE);
        buttonPane.add(this.aliveCellsLabel);
        buttonPane.add(this.start);
        buttonPane.add(this.stop);
        this.start.addActionListener(this::startActionPerformed);
        this.stop.addActionListener(this::stopActionPerformed);

        return buttonPane;
    }

    private void setActorEnvironment() {
        ActorSystem system = ActorSystem.create(ACTOR_SYSTEM_NAME);
        this.coordinatorActor = system.actorOf(Coordinator.props(this), COORDINATOR_ACTOR_NAME);
    }

    private void startActionPerformed(ActionEvent event) {
        start.setEnabled(false);
        stop.setEnabled(true);
        this.coordinatorActor.tell(new InitializeCellMessage(), ActorRef.noSender());
    }

    private void stopActionPerformed(ActionEvent event) {
        this.stopAction();
    }
}