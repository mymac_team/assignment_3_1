package gameOfLife.view;


import akka.actor.ActorRef;
import gameOfLife.Application;
import gameOfLife.model.counter.Counter;
import gameOfLife.model.counter.CounterImpl;
import gameOfLife.model.matrix.Matrix;
import gameOfLife.model.matrix.MatrixImpl;
import gameOfLife.model.message.Messages;

import javax.swing.*;
import java.awt.*;

public class MatrixPanel extends JPanel {

    private static final int SIZE = 10;
    private final ActorRef coordinatorActor;
    private final Counter counter = new CounterImpl();
    private Matrix matrix = new MatrixImpl.Builder()
                            .setSize(Application.getSize())
                            .setEmptyMatrix().build();

    MatrixPanel(ActorRef coordinatorActor) {
        this.coordinatorActor = coordinatorActor;
    }

    void updateModel(Matrix matrix) {
        this.matrix = matrix;
        this.counter.increment();
    }

    int getMatrixSideSize() {
        return this.matrix.getSize() * SIZE;
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        this.drawMatrixCells(graphics);
        this.fillMatrixCells(graphics);

        if(counter.getValue() > 0) {
            this.coordinatorActor.tell(Messages.NEXT_EVOLUTION, ActorRef.noSender());
            this.counter.decrement();
        }
    }

    private void fillMatrixCells(Graphics graphics) {
        graphics.setColor(Color.BLACK);
        graphics.drawRect(SIZE, SIZE, this.getMatrixSideSize(), this.getMatrixSideSize());

        for (int i = SIZE; i <= this.getMatrixSideSize(); i += SIZE) {
            graphics.drawLine(i, SIZE, i, this.getMatrixSideSize() + SIZE);
        }

        for (int i = SIZE; i <= this.getMatrixSideSize(); i += SIZE) {
            graphics.drawLine(SIZE, i, this.getMatrixSideSize() + SIZE, i);
        }
    }

    private void drawMatrixCells(Graphics graphics) {
        for (int row = 0; row < this.matrix.getSize(); row++) {
            for (int column = 0; column < this.matrix.getSize(); column++) {
                if (this.matrix.get(row, column)) {
                    int cellX = SIZE + (row * SIZE);
                    int cellY = SIZE + (column * SIZE);
                    graphics.setColor(Color.BLUE);
                    graphics.fillRect(cellX, cellY, SIZE, SIZE);
                }
            }
        }
    }
}
