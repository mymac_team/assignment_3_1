package gameOfLife.model.matrix;

/**
 * Models the matrix (the world representation) and the operations needed to its evolution.
 */
public interface Matrix {

    /**
     * Getter for the number of neighbours of a specified cell.
     * @param row The row of the cell.
     * @param column The column of the cell.
     * @return The number of neighbours of a specified cell.
     */
    int getNumberAliveNeighboursOf(int row, int column);

    /**
     * Getter for the value (alive or not) of a specified cell.
     * @param row The row of the cell.
     * @param column The column of the cell.
     * @return The value of a specified cell.
     */
    boolean get(int row, int column);

    /**
     * Getter for the width or the height of the (square) matrix.
     * @return The size of the matrix.
     */
    int getSize();

    /**
     * Updates the state of a cell in the matrix.
     * @param row The row of the cell.
     * @param column The column of the cell.
     * @param newValue The new state of a cell in the matrix.
     */
    void updateStateIn(int row, int column, boolean newValue);

    /**
     * Getter for the small matrix made by the 8 neighbours of a cell.
     * @param row The row of the cell.
     * @param column The column of the cell.
     * @return The matrix of the neighbours of a cell in the matrix.
     */
    Matrix getNeighbourMatrix(int row, int column);

    /**
     * Increments the number of the current number of alive cells in the matrix.
     */
    void incrementAliveCellsNumber();

    /**
     * Getter for the current number of alive cells in the matrix.
     * @return The current number of alive cells in the matrix.
     */
    long getAliveCellsNumber();

    /**
     * Resets the current number of alive cells in the matrix when an evolution is finished.
     */
    void resetAliveCellsNumber();

    /**
     * Getter for the number of cells in the matrix (size x size).
     * @return The number of cells in the matrix (size x size).
     */
    int getTotalCellsNumber();
}
