package gameOfLife.model.matrix;

import gameOfLife.model.counter.Counter;
import gameOfLife.model.counter.CounterImpl;

public class MatrixImpl implements Matrix {
    private static final int SMALL_MATRIX_CENTER_INDEX = 1;
    private static final int SMALL_MATRIX_SIZE = 3;
    private boolean[][] matrix;
    private final Counter numberOfAliveCells = new CounterImpl();

    private MatrixImpl(boolean[][] matrix) {
        this.matrix = matrix;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNumberAliveNeighboursOf(int row, int column) {
        int count = 0;

        for (Direction d : Direction.values()) {
            if (this.matrix[(this.getSize() + row + d.getX()) % this.getSize()][(this.getSize() + column + d.getY()) % this.getSize()]) {
                count++;
            }
        }

        return count;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean get(int row, int column) {
        return this.matrix[row][column];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSize() {
        return this.matrix.length;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateStateIn(int row, int column, boolean newValue) {
        if (this.get(row, column) != newValue) {
            this.matrix[row][column] = newValue;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Matrix getNeighbourMatrix(int row, int column) {
        Matrix neighbourWorld = new MatrixImpl.Builder()
                                                .setSize(SMALL_MATRIX_SIZE)
                                                .setEmptyMatrix().build();

        for (Direction d : Direction.values()) {
            boolean state = this.matrix[(this.getSize() + row + d.getX()) % this.getSize()][(this.getSize() + column + d.getY()) % this.getSize()];
            neighbourWorld.updateStateIn(SMALL_MATRIX_CENTER_INDEX + d.getX(), SMALL_MATRIX_CENTER_INDEX + d.getY(), state);
        }

        return neighbourWorld;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void incrementAliveCellsNumber() {
        this.numberOfAliveCells.increment();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getAliveCellsNumber() {
        return this.numberOfAliveCells.getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void resetAliveCellsNumber() {
        this.numberOfAliveCells.setValue(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getTotalCellsNumber() {
        return this.getSize() * this.getSize();
    }

    public static class Builder {

        private static final double FIFTY_PERCENT = 0.5;
        private int size;
        private boolean[][] matrix;

        /**
         * Default constructor of the builder.
         */
        public Builder() {

        }

        /**
         * Setter for the size of the matrix.
         * @param size of the matrix.
         * @return the builder itself.
         */
        public Builder setSize(int size) {
            this.size = size;
            return this;
        }

        /**
         * Setter for the matrix. It fill each cell with a random boolean value.
         * @return the builder itself.
         */
        public Builder setMatrix() {

            if (this.size == 0) {
                throw new IllegalStateException();
            } else {
                this.matrix = new boolean[this.size][this.size];

                for (int i = 0; i <= this.size - 1; i++) {
                    for (int j = 0; j <= this.size - 1; j++) {
                        this.matrix[i][j] = this.getRandomBoolean();
                    }
                }
            }

            return this;
        }

        public Builder setEmptyMatrix() {
            if (this.size != 0) {
                this.matrix = new boolean[this.size][this.size];
            } else {
                throw new IllegalStateException();
            }
            return this;
        }

        /**
         * Method that create a MatrixImpl if the matrix has been setted before in the builder.
         * @return the matrix implementation.
         */
        public MatrixImpl build() {
            if (this.matrix != null) {
                return new MatrixImpl(this.matrix);
            } else {
                throw new IllegalStateException();
            }
        }

        private boolean getRandomBoolean() {
            return Math.random() < FIFTY_PERCENT;
        }

    }
}
