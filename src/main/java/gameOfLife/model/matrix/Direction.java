package gameOfLife.model.matrix;

/**
 * The iteration direction from the center of a small matrix
 * (3x3) in which are contained the neighbour cells of a cell of the matrix.
 */
public enum Direction {
    NORTHWEST(-1, -1),
    NORTH(-1, 0),
    NORTHEAST(-1, 1),
    EAST(0, 1),
    SOUTHEAST(1, 1),
    SOUTH(1, 0),
    SOUTHWEST(1, -1),
    WEST(0, -1),
    CENTER(0, 0);

    private final int x;
    private final int y;

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public String toString() {
        return "Direction: (" + this.getX() + ", " + this.getY() + ")";
    }
}
