package gameOfLife.model.message;

import java.io.Serializable;

public enum Messages implements Serializable {
    NEXT_EVOLUTION,
    STOP_EVOLUTION;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}