package gameOfLife.model.message;

import java.io.Serializable;

public class InitializeCellMessage implements Serializable {
    private int row;
    private int column;
    private final static int DEFAULT_INDEX = 0;

    public InitializeCellMessage() {
        this.row = DEFAULT_INDEX;
        this.column = DEFAULT_INDEX;
    }

    public InitializeCellMessage(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return this.row;
    }

    public int getColumn() {
        return this.column;
    }

    public String toString() {
        return "InitializeCellMessage{" +
                "row=" + row +
                ", column=" + column +
                '}';
    }
}
