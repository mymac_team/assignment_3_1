package gameOfLife.model.message;

import java.io.Serializable;

/**
 * The message about a cell.
 * Whether the message is a request or a response or something else is defined in the concrete class.
 */
public abstract class AbstractCellToUpdateMessage implements Serializable {

    private final int row;
    private final int column;

    /**
     * Constructor that initialize the row and the column to a default value.
     * @param row The row of the cell in the matrix.
     * @param column The column of the cell in the matrix.
     */
    AbstractCellToUpdateMessage(final int row, final int column) {
        this.row = row;
        this.column = column;
    }

    public final int getRow() {
        return row;
    }

    public final int getColumn() {
        return column;
    }

    public String toString() {
        return "AbstractCellToUpdateMessage{"
                + "row=" + row
                + ", column=" + column
                + '}';
    }
}