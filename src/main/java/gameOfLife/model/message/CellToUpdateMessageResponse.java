package gameOfLife.model.message;

/**
 * {@inheritDoc}
 * Sets the updated state of a cell
 * (whether it is alive or not).
 */
public class CellToUpdateMessageResponse extends AbstractCellToUpdateMessage {
    private final boolean state;

    /**
     * {@inheritDoc}
     * Sets the updated state of a cell
     * (whether it is alive or not).
     */
    public CellToUpdateMessageResponse(final int row, final int column, final boolean newState) {
        super(row, column);
        this.state = newState;
    }

    public boolean getState() {
        return this.state;
    }

    public String toString() {
        return super.toString() + "CellToUpdateMessageResponse{"
                + "state=" + state
                + '}';
    }
}