package gameOfLife.model.message;

import gameOfLife.model.matrix.Matrix;

/**
 * {@inheritDoc}
 * Sets the matrix of the neighbours of a cell.
 */
public class CellToUpdateMessageRequest extends AbstractCellToUpdateMessage {
    private Matrix matrix;

    public CellToUpdateMessageRequest(final int row, final int column, final Matrix matrix) {
        super(row, column);
        this.matrix = matrix;
    }

    public Matrix getMatrix() {
        return this.matrix;
    }
}
