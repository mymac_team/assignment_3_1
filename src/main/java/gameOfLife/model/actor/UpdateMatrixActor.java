package gameOfLife.model.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.FromConfig;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import gameOfLife.model.message.CellToUpdateMessageRequest;
import gameOfLife.model.message.CellToUpdateMessageResponse;
import gameOfLife.model.counter.Counter;
import gameOfLife.model.counter.CounterImpl;

/**
 * The actor which manage the update of the matrix.
 * It sends the matrix updated to the coordinator {@link gameOfLife.controller.actor.Coordinator}
 */
public class UpdateMatrixActor extends AbstractIterateMatrixActor {
    private static final int DEFAULT_INDEX = 0;
    private final Counter cellUpdateCounter;
    private final ActorRef coordinatorActor;
    private ActorRef routerActor;

    /**
     * Factory for a new UpdateMatrixActor.
     * @param coordinatorActor the coordinator reference.
     * @return The Props.
     */
    public static Props props(ActorRef coordinatorActor) {
        return Props.create(UpdateMatrixActor.class, () -> new UpdateMatrixActor(coordinatorActor));
    }

    /**
     * The constructor of the actor which manage the update of the matrix.
     * @param coordinatorActor The coordinator actor.
     */
    public UpdateMatrixActor(ActorRef coordinatorActor) {
        this.coordinatorActor = coordinatorActor;
        this.cellUpdateCounter = new CounterImpl(super.getMatrix().getTotalCellsNumber());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void preStart() {
        Config config = ConfigFactory.parseString("akka.actor.deployment {\n" +
                "    /router {\n" +
                "      router = round-robin-pool\n" +
                "      nr-of-instances = " + super.getMatrix().getSize() + "\n" +
                "    }\n" +
                "  }\n");
        ActorSystem routerSystem = ActorSystem.create("RouterSystem", ConfigFactory.load(config));

        this.routerActor = routerSystem.actorOf(FromConfig.getInstance().props(CellUpdater.props(getSelf())),
                "router");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CellToUpdateMessageRequest.class, message -> {
                    if(this.cellUpdateCounter.isReached(0)) {
                        this.cellUpdateCounter.setValue(super.getMatrix().getTotalCellsNumber());
                    }
                    this.routerActor.tell(message, getSelf());
                })
                .match(CellToUpdateMessageResponse.class, message -> {
                    if (this.cellUpdateCounter.getValue() > 0) {
                        super.getMatrix().updateStateIn(message.getRow(), message.getColumn(), message.getState());
                        this.cellUpdateCounter.decrement();

                        if (this.cellUpdateCounter.isReached(0)) {
                            this.sendSelfMessage(DEFAULT_INDEX, DEFAULT_INDEX);
                        }
                    } else {
                        this.notifyAndIterate(this.coordinatorActor, message);
                    }
                })
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void sendSelfMessage(int row, int column) {
        boolean state = super.getMatrix().get(row, column);
        getSelf().tell(new CellToUpdateMessageResponse(row, column, state), ActorRef.noSender());
    }
}
