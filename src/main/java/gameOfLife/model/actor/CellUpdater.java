package gameOfLife.model.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import gameOfLife.model.matrix.Direction;
import gameOfLife.model.message.CellToUpdateMessageResponse;
import gameOfLife.model.message.CellToUpdateMessageRequest;
import gameOfLife.model.matrix.Matrix;

public class CellUpdater extends AbstractActor {

    private static final int OVER_POPULATION_LIMIT = 4;
    private static final int SURVIVING_MIN_NEIGHBOURS = 2;
    private static final int SURVIVING_MAX_NEIGHBOURS = 3;
    private static final int INDEX_CENTER_SMALL_MATRIX = 1;

    private final ActorRef updateMatrixActor;

    /**
     * Factory method for the actor which will update a cell in its evolution.
     * @param updateMatrixActor {@link gameOfLife.model.actor.UpdateMatrixActor}.
     * @return the Props itself.
     */
    public static Props props(ActorRef updateMatrixActor) {
        return Props.create(CellUpdater.class, () -> new CellUpdater(updateMatrixActor));
    }

    private CellUpdater(ActorRef updateMatrixActor) {
        this.updateMatrixActor = updateMatrixActor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CellToUpdateMessageRequest.class, message -> {
                    Matrix neighboursMatrix = message.getMatrix();
                    boolean isAlive = neighboursMatrix.get(INDEX_CENTER_SMALL_MATRIX, INDEX_CENTER_SMALL_MATRIX);

                    if (this.isDying(neighboursMatrix, isAlive)) {
                        isAlive = false;
                    } else if (isLiving(!isAlive, livingNeighbourFromCenterCell(neighboursMatrix), SURVIVING_MAX_NEIGHBOURS)) {
                        isAlive = true;
                    }

                    updateMatrixActor.tell(new CellToUpdateMessageResponse(message.getRow(), message.getColumn(), isAlive), getSelf());
                })
                .build();
    }

    private boolean isLiving(boolean b, int i, int survivingMaxNeighbours) {
        return b && i == survivingMaxNeighbours;
    }

    private boolean isDying(Matrix neighboursMatrix, boolean isAlive) {
        return isAlive && ((livingNeighbourFromCenterCell(neighboursMatrix) >= OVER_POPULATION_LIMIT) ||
                (0 <= livingNeighbourFromCenterCell(neighboursMatrix) &&
                        livingNeighbourFromCenterCell(neighboursMatrix) < SURVIVING_MIN_NEIGHBOURS));
    }

    private int livingNeighbourFromCenterCell(Matrix matrix) {
        return this.livingNeighbour(INDEX_CENTER_SMALL_MATRIX, INDEX_CENTER_SMALL_MATRIX, matrix);
    }

    private int livingNeighbour(int row, int column, Matrix matrix) {
        int livingNeighbour = 0;

        for(Direction direction : Direction.values()) {
            boolean isCenter = isLiving(direction.getX() == 0, direction.getY(), 0);

            if(!isCenter && this.isNeighbourAlive(row, column, matrix, direction)) {
                livingNeighbour++;
            }
        }

        return livingNeighbour;
    }

    private boolean isNeighbourAlive(int row, int column, Matrix matrix, Direction direction) {
        int neighbourRow = row + direction.getX();
        int neighbourColumn = column + direction.getY();

        neighbourRow = this.getRightIndex(matrix, neighbourRow);
        neighbourColumn = this.getRightIndex(matrix, neighbourColumn);

        return matrix.get(neighbourRow, neighbourColumn);
    }

    private int getRightIndex(Matrix matrix, int index) {
        return index < 0 ? matrix.getSize() - 1 :
                index == matrix.getSize() ? 0 : index;
    }
}
