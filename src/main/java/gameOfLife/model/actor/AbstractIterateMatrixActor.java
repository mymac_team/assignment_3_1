package gameOfLife.model.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import gameOfLife.Application;
import gameOfLife.model.message.AbstractCellToUpdateMessage;
import gameOfLife.model.matrix.Matrix;
import gameOfLife.model.matrix.MatrixImpl;

/**
 * Abstract actor which is used to iterate the matrix and notify another actor
 * to do something according to its implementation.
 */
public abstract class AbstractIterateMatrixActor extends AbstractActor {

    private static final int DEFAULT_COLUMN = 0;

    private Matrix matrix = new MatrixImpl.Builder()
            .setSize(Application.getSize())
            .setEmptyMatrix()
            .build();

    /**
     * Sends a message to the actor itself about an update or request
     * according to its concrete class.
     * @param row The cell row.
     * @param column The cell column.
     */
    protected abstract void sendSelfMessage(final int row, final int column);

    /**
     * Notify the actor and iterate over the matrix to perform a new
     * notification.
     * @param actor the actor which will be notified.
     * @param message the message about the cell which will be examined
     * in the concrete class.
     */
    protected void notifyAndIterate(final ActorRef actor, final AbstractCellToUpdateMessage message) {
        int row = message.getRow();
        int column = message.getColumn();

        actor.tell(message, getSelf());

        if (isLastCellNotReached(row, column)) {
            row++;
            column = DEFAULT_COLUMN;
            this.sendSelfMessage(row, column);
        } else if (!this.isLastColumnReached(column)) {
            column++;
            this.sendSelfMessage(row, column);
        }
    }

    /**
     * Returns whether the last column is reached but not the last row, so the iteration can go on.
     * @param column the column of a cell in the matrix.
     * @return Whether the last column is reached but not the last row, so the iteration can go on.
     */
    protected boolean isLastColumnReached(final int column) {
        return column == this.getMatrix().getSize() - 1;
    }

    protected Matrix getMatrix() {
        return this.matrix;
    }

    private boolean isLastCellNotReached(final int row, final int column) {
        return column == this.matrix.getSize() - 1
                && row < this.matrix.getSize() - 1;
    }
}
