package gameOfLife.model.counter;

public class CounterImpl implements Counter {
    private static final long DEFAULT_VALUE = 0;
    private long counter;

    public CounterImpl() {
        this(DEFAULT_VALUE);
    }

    public CounterImpl(long counter) {
        this.counter = counter;
    }

    @Override
    public long getValue() {
        return this.counter;
    }

    @Override
    public void decrement() {
        this.counter--;
    }

    @Override
    public void increment() {
        this.counter++;
    }

    @Override
    public void setValue(int value) {
        this.counter = value;
    }

    @Override
    public boolean isReached(long value) {
        return this.counter == value;
    }

    @Override
    public String toString() {
        return "Counter: " + this.counter;
    }
}
