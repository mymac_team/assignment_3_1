package gameOfLife.model.counter;

public interface Counter {
    long getValue();

    void decrement();

    void increment();

    void setValue(int value);

    boolean isReached(long value);
}
